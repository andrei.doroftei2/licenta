#!/bin/bash
export GITLAB_HOME=/opt/gitlab
docker compose up -d
docker exec -it gitlab-ee grep 'Password:' /etc/gitlab/initial_root_password
chown -R administrator:administrator /home/administrator
