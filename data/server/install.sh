#!/bin/bash
apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository -y "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
usermod -aG docker administrator
systemctl enable docker
docker --version
docker compose --version