locals {
  json_users    = jsondecode(file("../data/ldap.json"))
  user_emails   = distinct([for user in local.json_users : user["E-mail"]])
  user_filter   = [for user in data.ldap_user.users : "userPrincipalName=${user.user_principal_name}"]
  users         = [for user in data.ldap_user.users : user.user_principal_name]
  modified_file = replace(data.local_file.input.content, "/.*user_filter.*/", "   'user_filter' => '(&(|(employeeType=Student)(employeeType=Masterand))(|(${join(")(", local.user_filter)})))',")
}

data "local_file" "input" {
  filename = "../data/gitlab.rb"
}

resource "local_file" "output" {
  content  = local.modified_file
  filename = "../data/gitlab.rb"
  provisioner "local-exec" {
    command = "scp ../data/gitlab.rb administrator@${var.server_ip}:/home/administrator/gitlab_server/gitlab.rb"
  }
}



data "ldap_user" "users" {
  for_each = {
    for email in local.user_emails : email => {
      ou                  = var.ldap_ou
      user_principal_name = email
    } if email != null
  }
  ou                  = each.value["ou"]
  user_principal_name = each.value["user_principal_name"]
}



# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# resource "ldap_group" "group" {
#   ou   = "OU=CALC-II,OU=Licență - CALCULATOARE,OU=FACULTATEA DE INGINERIE ELECTRICĂ ȘI ȘTIINȚA CALCULATOARELOR,OU=Studenti,DC=usv,DC=intra"
#   name = "Gitlab_test_group"
#   members = [
#     for email, user in data.ldap_user.users : user.id
#   ]
#   description = "This group contains users that are able to login to Gitlab."
# }
