resource "gitlab_group" "main_group" {
  name = "FIESC"
  path = "fiesc"
}

resource "gitlab_group" "secondary_groups" {
  for_each               = var.study_programs
  name                   = each.value
  path                   = each.value
  parent_id              = gitlab_group.main_group.id
  visibility_level       = "private"
  share_with_group_lock  = true
  project_creation_level = "developer"
}

data "gitlab_group" "group" {
  full_path = "fiesc"
  depends_on = [
    gitlab_group.main_group
  ]
}
