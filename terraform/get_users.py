import pandas as pd

df = pd.read_excel('../data/ldap.xlsx')

json_data = df.to_json(orient='records', indent=4)

with open('../data/ldap.json', 'w') as outfile:
    outfile.write(json_data)
