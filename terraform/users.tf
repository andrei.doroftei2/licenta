resource "gitlab_user" "users" {
  for_each          = toset(local.users)
  name              = "${split(".", split("@", each.key)[0])[0]} ${split(".", split("@", each.key)[0])[1]}"
  username          = split("@", each.key)[0]
  email             = each.key
  reset_password    = true
  skip_confirmation = true
  projects_limit    = 100000
}



resource "gitlab_user" "admin_users" {
  for_each         = var.admin_users
  name             = "${each.value.first_name} ${each.value.last_name}"
  is_admin         = true
  username         = split("@", each.value["email"])[0]
  email            = each.value.email
  reset_password   = true
  can_create_group = true
}

# data "http" "add_users_to_groups" {
#   # for_each = {
#   #   for user in local.intersection : user.user_principal_name => user
#   # }
#   for_each = { for u, user_data in var.users : u => [user_data, gitlab_group.secondary_groups[user_data.group]] if user_data.group != "" }
#   url      = "http://${var.server_ip}/api/v4/groups/${each.value[1].id}/members"
#   method   = "POST"
#   request_headers = {
#     Accept        = "application/json"
#     PRIVATE-TOKEN = var.gitlab_token
#     Content-Type  = "application/json"
#   }
#   request_body = jsonencode({
#     user_id      = gitlab_user.users[each.key].id
#     access_level = 30
#   })
#   depends_on = [
#     gitlab_group.secondary_groups, gitlab_user.users
#   ]
# }

# data "http" "add_admin_users_to_group" {
#   for_each = var.admin_users
#   url      = "http://${var.server_ip}/api/v4/groups/${gitlab_group.main_group.id}/members"
#   method   = "POST"
#   request_headers = {
#     Accept        = "application/json"
#     PRIVATE-TOKEN = var.gitlab_token
#     Content-Type  = "application/json"
#   }
#   request_body = jsonencode({
#     user_id      = gitlab_user.admin_users[each.key].id
#     access_level = 50
#   })
#   depends_on = [
#     gitlab_group.main_group, gitlab_user.admin_users
#   ]
# }

data "gitlab_users" "users" {
  search = ""
}
