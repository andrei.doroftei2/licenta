variable "study_programs" {
  type        = set(string)
  description = "List of study programs"
}

variable "ldap_bind_user" {
  type        = string
  description = "The user to use when connecting to LDAP"
}

variable "ldap_bind_password" {
  type        = string
  description = "The password to use when connecting to LDAP"
}

variable "ldap_host" {
  type        = string
  description = "The host address"
}

variable "ldap_ou" {
  type        = string
  description = "Variable that contains the OU to use"
}

variable "admin_users" {
  type = map(object({
    first_name = string
    last_name  = string
    email      = string
  }))
  description = "Variable to store admin users details"
}

variable "gitlab_token" {
  type        = string
  description = "The token Terraform uses to deploy the infrastructure on Gitlab"
  sensitive   = true
}

variable "gitlab_runner_token" {
  type        = string
  description = "The token Gitlab runners use to register"
  sensitive   = true
}

variable "number_of_gitlab_runners" {
  type        = number
  description = "It represents the number of gitlab runners"
}

variable "server_ip" {
  type        = string
  description = "The ip of the Gitlab server"
}
