resource "gitlab_project" "template_project" {
  name               = "C++ Project"
  description        = "Template project for C/C++ applications"
  visibility_level   = "public"
  build_git_strategy = "clone"
}

resource "gitlab_repository_file" "template_project_file" {
  for_each            = fileset("../data/template_project_files/", "**/*")
  project             = gitlab_project.template_project.id
  file_path           = each.value
  branch              = "main"
  content             = file("../data/template_project_files/${each.value}")
  author_email        = "usv@admin.com"
  author_name         = "usv"
  commit_message      = "feature: add ${each.value}"
  overwrite_on_create = true
}

resource "gitlab_group_project_file_template" "template_link" {
  group_id                 = data.gitlab_group.group.group_id
  file_template_project_id = gitlab_project.template_project.id
}

resource "gitlab_project" "user_projects" {
  for_each                        = gitlab_user.users
  name                            = "${each.value.name}_s Project"
  namespace_id                    = each.value.namespace_id
  description                     = "My awesome project"
  group_with_project_templates_id = gitlab_group_project_file_template.template_link.id
  forked_from_project_id          = gitlab_project.template_project.id
  request_access_enabled          = false
  issues_enabled                  = true
  merge_requests_enabled          = true
  wiki_enabled                    = true
  snippets_enabled                = true
  shared_runners_enabled          = true
  visibility_level                = "private"
  build_git_strategy              = "clone"
  depends_on = [
    gitlab_repository_file.template_project_file
  ]
}
# depends on data.http.add_users_to_groups
