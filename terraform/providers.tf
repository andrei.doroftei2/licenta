terraform {
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "15.9.0"
    }
    ldap = {
      source = "Ouest-France/ldap"
    }
  }
}

provider "gitlab" {
  base_url = "http://${var.server_ip}/api/v4/"
  token    = var.gitlab_token
}


provider "ldap" {
  host          = var.ldap_host
  port          = 389
  bind_user     = var.ldap_bind_user
  bind_password = var.ldap_bind_password
}
