resource "gitlab_runner" "basic_runner" {
  count              = var.number_of_gitlab_runners
  description        = "Runner used for pipelines"
  registration_token = var.gitlab_runner_token
  run_untagged       = true
}

locals {
  runner_tokens = {
    for idx in range(0, var.number_of_gitlab_runners) :
    idx => gitlab_runner.basic_runner[idx].authentication_token
  }
  runner_names = {
    for idx in range(0, var.number_of_gitlab_runners) : idx => idx
  }
  mmsd = merge(tomap(local.runner_names), tomap(local.runner_tokens))
}

resource "local_file" "config" {
  filename = "../data/runners_config/config.toml"
  content = templatefile("../data/runner_config.tftpl",
    {
      server_ip = var.server_ip
      tokens    = values(local.runner_tokens)
    }
  )
  provisioner "local-exec" {
    command = "scp ../data/runners_config/config.toml administrator@${var.server_ip}:/home/administrator/gitlab-runner/config.toml"
  }
}

resource "gitlab_application_settings" "gitlab_settings" {
  shared_runners_enabled                   = true
  shared_runners_minutes                   = 9999
  shared_runners_text                      = "Shared runners are working"
  require_admin_approval_after_user_signup = true
  signup_enabled                           = false
}
