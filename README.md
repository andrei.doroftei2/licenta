<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_gitlab"></a> [gitlab](#requirement\_gitlab) | 15.9.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_gitlab"></a> [gitlab](#provider\_gitlab) | 15.9.0 |
| <a name="provider_http"></a> [http](#provider\_http) | 3.2.1 |
| <a name="provider_local"></a> [local](#provider\_local) | 2.4.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [gitlab_application_settings.gitlab_settings](https://registry.terraform.io/providers/gitlabhq/gitlab/15.9.0/docs/resources/application_settings) | resource |
| [gitlab_group.main_group](https://registry.terraform.io/providers/gitlabhq/gitlab/15.9.0/docs/resources/group) | resource |
| [gitlab_group.secondary_groups](https://registry.terraform.io/providers/gitlabhq/gitlab/15.9.0/docs/resources/group) | resource |
| [gitlab_group_project_file_template.template_link](https://registry.terraform.io/providers/gitlabhq/gitlab/15.9.0/docs/resources/group_project_file_template) | resource |
| [gitlab_project.template_project](https://registry.terraform.io/providers/gitlabhq/gitlab/15.9.0/docs/resources/project) | resource |
| [gitlab_project.user_projects](https://registry.terraform.io/providers/gitlabhq/gitlab/15.9.0/docs/resources/project) | resource |
| [gitlab_repository_file.template_project_file](https://registry.terraform.io/providers/gitlabhq/gitlab/15.9.0/docs/resources/repository_file) | resource |
| [gitlab_runner.basic_runner](https://registry.terraform.io/providers/gitlabhq/gitlab/15.9.0/docs/resources/runner) | resource |
| [gitlab_user.admin_users](https://registry.terraform.io/providers/gitlabhq/gitlab/15.9.0/docs/resources/user) | resource |
| [gitlab_user.users](https://registry.terraform.io/providers/gitlabhq/gitlab/15.9.0/docs/resources/user) | resource |
| [local_file.config](https://registry.terraform.io/providers/hashicorp/local/latest/docs/resources/file) | resource |
| [gitlab_group.group](https://registry.terraform.io/providers/gitlabhq/gitlab/15.9.0/docs/data-sources/group) | data source |
| [gitlab_users.users](https://registry.terraform.io/providers/gitlabhq/gitlab/15.9.0/docs/data-sources/users) | data source |
| [http_http.add_admin_users_to_group](https://registry.terraform.io/providers/hashicorp/http/latest/docs/data-sources/http) | data source |
| [http_http.add_users_to_groups](https://registry.terraform.io/providers/hashicorp/http/latest/docs/data-sources/http) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_admin_users"></a> [admin\_users](#input\_admin\_users) | Variable to store admin users details | <pre>map(object({<br>    first_name = string<br>    last_name  = string<br>    email      = string<br>  }))</pre> | n/a | yes |
| <a name="input_gitlab_runner_token"></a> [gitlab\_runner\_token](#input\_gitlab\_runner\_token) | The token Gitlab runners use to register | `string` | n/a | yes |
| <a name="input_gitlab_token"></a> [gitlab\_token](#input\_gitlab\_token) | The token Terraform uses to deploy the infrastructure on Gitlab | `string` | n/a | yes |
| <a name="input_number_of_gitlab_runners"></a> [number\_of\_gitlab\_runners](#input\_number\_of\_gitlab\_runners) | It represents the number of gitlab runners | `number` | n/a | yes |
| <a name="input_study_programs"></a> [study\_programs](#input\_study\_programs) | List of study programs | `set(string)` | n/a | yes |
| <a name="input_users"></a> [users](#input\_users) | Variable to store basic users details | <pre>map(object({<br>    first_name = string<br>    last_name  = string<br>    email      = string<br>    group      = string<br>  }))</pre> | n/a | yes |

## Outputs

No outputs.
<!-- END_TF_DOCS -->